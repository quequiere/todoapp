package com.supinfo.todoapp.Tools;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

/*
 * Manage dialog alerts
 */
public class DialogAlertTool {
    // Create alert popup to display info to user
    public static void displayInfoPopup(AppCompatActivity activity, String title, String message)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setNeutralButton("ok",null);
        alert.create().show();
    }

    public static void displayDataLoadError(AppCompatActivity activity, String errorMessage)
    {
        String message = String.format("Can't load data from server.\nError: %s",errorMessage);
        displayInfoPopup(activity,"Loading data error",message);
    }
}
