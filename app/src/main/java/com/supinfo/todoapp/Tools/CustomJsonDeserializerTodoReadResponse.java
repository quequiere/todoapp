package com.supinfo.todoapp.Tools;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.supinfo.todoapp.models.Dto.TodoList;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkResponseRead;

import java.lang.reflect.Type;

/*
 * Deserialize a todoList read response
 */
public class CustomJsonDeserializerTodoReadResponse implements JsonDeserializer<NetworkResponseRead> {

    @Override
    public NetworkResponseRead deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        NetworkResponseRead response;

       if(json.isJsonObject())
        {
            JsonObject jsonObject = json.getAsJsonObject();
            if(jsonObject.has("success"))
            {
                response = new Gson().fromJson(jsonObject,NetworkResponseRead.class);
            }
            else
            {
                TodoList list = new Gson().fromJson(jsonObject,TodoList.class);
                response = new NetworkResponseRead();
                response.todolistResponse = list;
            }
        }
        else
        {
            response = new NetworkResponseRead();
            response.success = false;
            response.message = "Internal error from app while json deserialization";
        }

        return response;
    }
}
