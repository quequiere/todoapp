package com.supinfo.todoapp.Tools;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.supinfo.todoapp.models.Dto.TodoList;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkResponseTodoList;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * Deserialize a todoList response
 */
public class CustomJsonDeserializerTodoListResponse implements JsonDeserializer<NetworkResponseTodoList> {

    @Override
    public NetworkResponseTodoList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        NetworkResponseTodoList response;

        if(json.isJsonArray())
        {
            JsonArray array = json.getAsJsonArray();
            response = new NetworkResponseTodoList();
            TodoList[] list = new Gson().fromJson(array,TodoList[].class);
            response.list = new ArrayList<>(Arrays.asList(list)) ;

        }
        else if(json.isJsonObject())
        {
            JsonObject jsonObject = json.getAsJsonObject();
            response = new Gson().fromJson(jsonObject,NetworkResponseTodoList.class);
        }
        else
        {
            response = new NetworkResponseTodoList();
            response.success=false;
            response.message = "Internal error from app while json deserialization";
        }

        return response;
    }
}
