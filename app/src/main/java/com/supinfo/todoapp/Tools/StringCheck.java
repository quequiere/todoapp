package com.supinfo.todoapp.Tools;

import java.util.regex.Pattern;

// Simple string checker class
public class StringCheck {
    public static boolean isTooLong(String s)
    {
        return s.length()>25;
    }

    public static boolean isAlphaNumeric(String s)
    {
        Pattern p = Pattern.compile("[^a-zA-Z0-9]");
        return p.matcher(s).find();
    }

    public static boolean isEmailValid(String s)
    {
        Pattern p = Pattern.compile( "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$", Pattern.CASE_INSENSITIVE);
        return !p.matcher(s).matches();
    }
}
