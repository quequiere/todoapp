package com.supinfo.todoapp.networking;

import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseCreateTodoList;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseLogin;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseLogout;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseRegister;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseUpdatedtodo;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkResponseRead;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkResponseTodoList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/*
 * Interface to define HTTP operations using Retrofit annotations
 * All requests types and parameters included in this interface
 * All actions destination to various controllers
 */
public interface RestApiRoutes {

    @GET("suptodo/?action=list")
    Call<NetworkResponseTodoList> getAllTodoList();

    @GET("suptodo/?action=login")
    Call<NetworkReponseLogin> tryLogin();

    @GET("suptodo/?action=logout")
    Call<NetworkReponseLogout> tryLogout();

    @GET("suptodo/?action=register")
    Call<NetworkReponseRegister> tryRegisterUser(
            @Query("firstname") String firstname,
            @Query("lastname") String lastname,
            @Query("email") String email
    );

    @GET("suptodo/?action=update")
    Call<NetworkReponseUpdatedtodo> updatedTodoList(
            @Query("id") String todoId,
            @Query("todo") String todoText
    );

    @GET("suptodo/?action=read")
    Call<NetworkResponseRead> readTodoList(
            @Query("id") String todoId
    );

    @GET("suptodo/?action=share")
    Call<NetworkReponseCreateTodoList> createNewTodoList(
            @Query("user") String friend
    );
}
