package com.supinfo.todoapp.networking.retrofit;

import android.support.v7.app.AppCompatActivity;

import com.google.gson.GsonBuilder;
import com.supinfo.todoapp.Tools.CustomJsonDeserializerTodoListResponse;
import com.supinfo.todoapp.Tools.CustomJsonDeserializerTodoReadResponse;
import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.configs.Routes;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkResponseRead;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkResponseTodoList;
import com.supinfo.todoapp.networking.RestApiRoutes;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestAPIClient {
    // Singleton to get same instance of client each time it is called
    private static Retrofit retrofit;
    private static RestApiRoutes routes;

    // Retrieve retrofit instance in order to make API requests
    private static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new CustomInterceptor());

            // Very special deserialization for NetworkdResponseTodolist cause REST API directly return array instead of return array inside object like all other request
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder
                    .registerTypeAdapter(NetworkResponseTodoList.class, new CustomJsonDeserializerTodoListResponse())
                    .registerTypeAdapter(NetworkResponseRead.class, new CustomJsonDeserializerTodoReadResponse());

            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(Routes.baseUrlApi)
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .client(httpClient.build())
                    .build();
        }

        return retrofit;
    }

    // Retrieve routes from dedicated interface
    public static RestApiRoutes getService() {
        if (routes == null) {
            routes = getRetrofitInstance().create(RestApiRoutes.class);
        }

        return routes;
    }

    // Generic request to handle all possible calls and error messages
    public static <T> void sendRequest(AppCompatActivity activity, Call<T> apiCall, CustomCallBack<T> callback) {
        apiCall.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                try
                {
                    if (response.isSuccessful()) {
                        callback.requestExectued(response.body());
                    } else {
                        DialogAlertTool.displayDataLoadError(activity, response.code() + " :" + response.message());
                        System.out.println("Error response while loaded data: " + response + "from url: " + call.request().url());
                        System.out.println("Server answered: " + response.body());
                    }
                }catch (NullPointerException e)
                {
                    DialogAlertTool.displayDataLoadError(activity, response.code() + " :" + response.message());
                    System.out.println("Error response while loaded data: " + response + "from url: " + call.request().url());
                    System.out.println("Server answered: " + response.body());
                }

            }

            @Override
            public void onFailure(Call<T> call, Throwable error) {
                DialogAlertTool.displayDataLoadError(activity, error.getMessage());
                System.out.println("Fatal error when call rest api: " + error.getMessage() + "from url: " + call.request().url());
                System.out.println(error);
                error.printStackTrace();
                callback.requestExectued(null);
            }
        });
    }
}
