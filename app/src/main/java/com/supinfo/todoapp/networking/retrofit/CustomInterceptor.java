package com.supinfo.todoapp.networking.retrofit;

import com.supinfo.todoapp.controllers.CredentialController;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class CustomInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        HttpUrl.Builder builder = originalHttpUrl.newBuilder();

        if(CredentialController.get().getUsername().length()>0){
            builder.addQueryParameter("username",CredentialController.get().getUsername() );
        }

        if(CredentialController.get().getPassword().length()>0)
        {
            builder.addQueryParameter("password", CredentialController.get().getPassword());
        }

        HttpUrl url = builder.build();

        Request.Builder requestBuilder = original.newBuilder().url(url);
        Request request = requestBuilder.build();

       System.out.println("Retrofit requested: "+request.url().toString());

        return chain.proceed(request);
    }
}
