package com.supinfo.todoapp.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.supinfo.todoapp.database.DatabaseTodo;
import com.supinfo.todoapp.database.SqlRequest;
import com.supinfo.todoapp.models.Dto.CredentialPair;

/*
 * DatabaseController here to manage all access to database
 */
public class DatabaseController {

    private static DatabaseController instance;
    private SQLiteDatabase database;
    private DatabaseTodo helper;

    // Get databaseController instance
    public static DatabaseController get()
    {
        if(instance==null)
        {
            instance=new DatabaseController();
        }

        return instance;
    }

    // Database initialization
    public void initializeDb(Context contex)
    {
        this.helper = new DatabaseTodo(contex);
        this.open();
    }

    private void open() throws SQLException {
        database = helper.getWritableDatabase();
    }

    // Store credentials
    public void saveCredentials(String username, String password)
    {
        // To be sure that we have only one value
        clearDatase();

        ContentValues values = new ContentValues();
        values.put("username", username);
        values.put("password", password);

       database.insert(SqlRequest.tbName, null, values);
    }

    // Retrieve credentials
    public CredentialPair getCredentialFromDb()
    {
        String[] cols = {"*"};
        Cursor cursor = database.query(SqlRequest.tbName, cols, null, null,null, null, null);

        cursor.moveToFirst();

        String username = cursor.getString(0);
        String password = cursor.getString(1);

      return new CredentialPair(username,password);
    }

    // Verify if credentials present in db
    public boolean hasCredentialInDb()
    {
        String[] cols = {"*"};
        Cursor cursor = database.query(SqlRequest.tbName, cols, null, null,null, null, null);

        return cursor.getCount()>0;
    }

    // Clear data in database
    public void clearDatase()
    {
        database.delete(SqlRequest.tbName,null,null);
    }
}
