package com.supinfo.todoapp.controllers;

import android.support.v7.app.AppCompatActivity;

import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.gui.activity.MainScreen;
import com.supinfo.todoapp.gui.helper.DisplayTodoHelper;
import com.supinfo.todoapp.gui.helper.NavBarHelper;
import com.supinfo.todoapp.models.Dto.TodoList;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseCreateTodoList;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseUpdatedtodo;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkResponseRead;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkResponseTodoList;
import com.supinfo.todoapp.networking.retrofit.RestAPIClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;

/*
 * TodoController here to manage all actions related to todos
 */
public class TodoController {

    private static TodoController controller;
    private List<TodoList> currentList;
    private static DisplayTodoHelper displayTodoHelper = new DisplayTodoHelper();

    // Retrieve TodoController instance
    public static TodoController get() {
        if (controller == null) {
            controller = new TodoController();
        }

        return controller;
    }

    public static DisplayTodoHelper getDisplayTodoHelper() {
        return displayTodoHelper;
    }

    public void setTodoList(List<TodoList> list) {
        currentList = list;
    }

    // Get the cached todolist and make it unmodifiable
    public List<TodoList> getCurrentTodoList() {
        if (currentList == null) {
            currentList = new ArrayList<>();
        }

        return Collections.unmodifiableList(currentList);
    }

    // Refresh the cached current list
    public void refreshedAllCachedTodoList(AppCompatActivity activity) {
        Call<NetworkResponseTodoList> call = RestAPIClient.getService().getAllTodoList();
        RestAPIClient.sendRequest(activity, call, object ->
        {
            if(object==null)
            {
                System.out.println("Failed to refreshed todo list ==> No internet !");
            }
            else if (object.success) {
                setTodoList(object.list);
                NavBarHelper.refreshTodoBar();

                // Use only when we are already displaying main screen
                if(activity instanceof  MainScreen)
                {
                    if(TodoController.getDisplayTodoHelper().getCurrentDisplayedTodolist()!=null)
                    {
                        TodoController.getDisplayTodoHelper().displayTodoListById((MainScreen) activity,TodoController.getDisplayTodoHelper().getCurrentDisplayedTodolist().getId());
                    }
                }

                System.out.println("Refreshed todolist, loaded number: " + object.list.size());
            } else {
                System.out.println("Failed to refreshed todo list: " + object.message);
            }
        });
    }

    // Update todo action
    public void sendUpdateSpecificTodoList(MainScreen activity, TodoList todo) {
        String valueToSend = null;

        if(todo.getTodoStringUnsplited().length()<=0)
        {
            DialogAlertTool.displayInfoPopup(activity,"Failed","Sorry, but the REST API doesn't support the update of empty list!\nSee php api line 495 for more detail !");
            return;
        }
        else
        {
            valueToSend = todo.getTodoStringUnsplited();
        }

        Call<NetworkReponseUpdatedtodo> call = RestAPIClient.getService().updatedTodoList(todo.getId(),valueToSend);

        String finalValueToSend = valueToSend;
        RestAPIClient.sendRequest(activity, call, object ->
        {
            if(object == null)
            {
                System.out.println("Fail to connect on web");
            }
            else if (object.success)
            {
                System.out.println("Success :"+ finalValueToSend);
//                DialogAlertTool.displayInfoPopup(activity,"Success","Updated task in todo list id "+todo.getId()+" !");
                refreshAndPrintTodolist(activity,todo.getId());
            }
            else {
                DialogAlertTool.displayInfoPopup(activity,"Fail","Failed to update todolist "+todo.getId()+" message: " +object.message);
            }
        });
    }

    // Read todo action
    public void refreshAndPrintTodolist(MainScreen activity, String todoId) {
        Call<NetworkResponseRead> call = RestAPIClient.getService().readTodoList(todoId);
        RestAPIClient.sendRequest(activity, call, object ->
        {
            if(object==null)
            {
                System.out.println("Failed to refresh from web");
            }
            else if (object.success)
            {
                System.out.println("Success refresh todo list:"+object.todolistResponse.getId());

                for(int x = 0; x<currentList.size();x++)
                {
                    TodoList scan = currentList.get(x);
                    if(scan.getId().equals(object.todolistResponse.getId()))
                    {
                        currentList.remove(x);
                        currentList.add(object.todolistResponse);
                    }
                }

                //when the current list displayed is the one refreshed, so we need to update the screen
                TodoList currentScreened = TodoController.getDisplayTodoHelper().getCurrentDisplayedTodolist();
                if(currentScreened!=null && currentScreened.getId().equals(object.todolistResponse.getId()))
                {
                    TodoController.getDisplayTodoHelper().displayTodoListById(activity,todoId);
                }

            }
            else {
                DialogAlertTool.displayInfoPopup(activity,"Failed","Failed to read todolist id: "+todoId+", message: "+object.message);
            }
        });
    }

    // Create a new shared todo action
    public void createNewTodoListShared(MainScreen activity, String friendName) {
        Call<NetworkReponseCreateTodoList> call = RestAPIClient.getService().createNewTodoList(friendName);
        RestAPIClient.sendRequest(activity, call, object ->
        {
            if(object==null)
            {
                System.out.println("Fail to connect on web");
            }
            else if (object.success)
            {
                System.out.println("Success created new todo list");
                this.refreshedAllCachedTodoList(activity);
                DialogAlertTool.displayInfoPopup(activity,"Success","New list was created !");

            }
            else {
                DialogAlertTool.displayInfoPopup(activity,"Fail","Failed to create new todo list.\nDetail: " +object.message);
            }
        });
    }
}
