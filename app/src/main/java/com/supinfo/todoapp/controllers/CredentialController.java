package com.supinfo.todoapp.controllers;

import android.support.v7.app.AppCompatActivity;

import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.models.Dto.User;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseLogin;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseLogout;
import com.supinfo.todoapp.models.NetworkResponseModels.NetworkReponseRegister;
import com.supinfo.todoapp.networking.retrofit.CustomCallBack;
import com.supinfo.todoapp.networking.retrofit.RestAPIClient;

import retrofit2.Call;

/*
 * CredentialController here to manage all actions related to identification
 */
public class CredentialController {

    private static CredentialController instance;
    private User currentUser;

    private String username;
    private String password;

    private CredentialController(String username, String password) {
        this.username = username;
        this.password = password;
    }

    // Retrieve CredentialController instance
    public static CredentialController get()
    {
        if(instance == null)
        {
            instance = new CredentialController("","");
        }

        return instance;
    }

    // Getters / setters
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void logout()
    {
        // remove data from cached memory
        this.setCurrentUser(null);
        this.username=null;
        this.password=null;

        instance = null;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    // Login action
    public static void tryAuthentificate(AppCompatActivity activity, String username, String password, CustomCallBack<Boolean> callBack)
    {
        // Create controller immediately cause we need to send username and password with interceptor
        instance = new CredentialController(username,password);

        Call<NetworkReponseLogin> call = RestAPIClient.getService().tryLogin();
        RestAPIClient.sendRequest(activity,call,object ->
        {
            if(object == null)
            {
                callBack.requestExectued(false);
            }
            else if(object.success)
            {
                System.out.println("Sucessfuly logged in !");

                User u = new User(object.id,object.username,object.password,object.firstname,object.lastname,object.email);

                instance.setCurrentUser(u);
                callBack.requestExectued(true);
            }

            else
            {
                callBack.requestExectued(false);
            }
        });

    }

    // Register action
    public static void tryRegister(AppCompatActivity activity, String username, String password, String firstname, String lastname, String email, CustomCallBack<NetworkReponseRegister> callBack)
    {
        // Create controller immediately cause we need to send username and password with interceptor
        instance= new CredentialController(username,password);

        Call<NetworkReponseRegister> call = RestAPIClient.getService().tryRegisterUser(firstname,lastname,email);
        RestAPIClient.sendRequest(activity,call,object ->
        {
            if(object==null)
            {
                NetworkReponseRegister resp = new NetworkReponseRegister();
                resp.success = false;
                resp.message = "Failed to connect on servers !";
                callBack.requestExectued(resp);
            }
            if(object.success)
            {
                System.out.println("Sucessfuly created account");

                User u = new User(object.id,object.username,object.password,object.firstname,object.lastname,object.email);

                instance.setCurrentUser(u);
                callBack.requestExectued(object);
            }

            else
            {
                callBack.requestExectued(object);
            }
        });

    }

    // Logout action
    public static void tryLogout(AppCompatActivity activity)
    {
        Call<NetworkReponseLogout> call = RestAPIClient.getService().tryLogout();
        RestAPIClient.sendRequest(activity,call,object ->
        {
            if(object==null)
            {
                DialogAlertTool.displayInfoPopup(activity,"Failed","Failed to logout from web");
            }
            else if(!object.success)
            {
                DialogAlertTool.displayInfoPopup(activity,"Failed","Failed to logout. Details:\n"+object.message);
            }
        });
    }
}
