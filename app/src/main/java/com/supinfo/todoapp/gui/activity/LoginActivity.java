package com.supinfo.todoapp.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.supinfo.todoapp.R;
import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.controllers.CredentialController;
import com.supinfo.todoapp.controllers.DatabaseController;
import com.supinfo.todoapp.controllers.TodoController;
import com.supinfo.todoapp.models.Dto.CredentialPair;

/*
 * Login view
 */
public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DatabaseController.get().initializeDb(this);

        // Remove tittle bar to get clear activity
        requestWindowFeature(Window.FEATURE_NO_TITLE); // Will hide the title
        getSupportActionBar().hide(); // Hide the title bar

        // ... and remove the notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);

        if(DatabaseController.get().hasCredentialInDb())
        {
            loginProcess(true);
        }

        // Register event when login button fired
        final Button button = findViewById(R.id.loginButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loginButtonClick(v);
            }
        });
    }

    // Open create account view / activity
    public void onCreateAccountClick(View v) {
        Intent intent = new Intent(this, CreateAccountScreen.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // Do nothing cause we haven't screen before
    }

    public void loginButtonClick(View view) {
        loginProcess(false);
    }

    public void loginProcess(boolean directLog) {
        AppCompatButton button =  this.<AppCompatButton>findViewById(R.id.loginButton);

        String usernameToUse = null;
        String passwordToUse = null;
        boolean databaseWasUsed = false;

        if(!directLog)
        {
            TextInputEditText usernameField = findViewById(R.id.usernameField);
            AppCompatEditText passwordField = (this.<AppCompatEditText>findViewById(R.id.passwordField));

            // Verify if all fields are filled
            if (usernameField == null || passwordField == null) {
                DialogAlertTool.displayInfoPopup(this, "Login error", "Internal error, can't find field");
                return;
            }

            if (usernameField.getText().length() <= 0 || passwordField.getText().length() <= 0) {
                DialogAlertTool.displayInfoPopup(this, "Login error", "Please provide a username and password.");
                return;
            }

            button.setEnabled(false);

            usernameToUse =  usernameField.getText().toString();
            passwordToUse =  passwordField.getText().toString();
        }
        else
        {
            CredentialPair pair = DatabaseController.get().getCredentialFromDb();
            usernameToUse = pair.username;
            passwordToUse = pair.password;
            databaseWasUsed = true;
        }

        boolean finalDatabaseWasUsed = databaseWasUsed;
        String finalUsernameToUse = usernameToUse;
        String finalPasswordToUse = passwordToUse;
        CredentialController.tryAuthentificate(this,usernameToUse,passwordToUse, success -> {

            if (success)
            {
                if(!finalDatabaseWasUsed)
                {
                    DatabaseController.get().saveCredentials(finalUsernameToUse, finalPasswordToUse);
                    System.out.println("Credentials stored in database !");
                }

                TodoController.get().refreshedAllCachedTodoList(this);
                Intent intent = new Intent(this, MainScreen.class);
                startActivity(intent);
            } else {

                if(finalDatabaseWasUsed)
                {
                    DialogAlertTool.displayInfoPopup(this, "Login failed", "Your stored credentials are not valid, they will be removed from local data");
                    DatabaseController.get().clearDatase();
                }
                else
                {
                    DialogAlertTool.displayInfoPopup(this, "Login failed", "Sorry, verify your username and password");
                }
            }

            button.setEnabled(true);
        });


    }
}
