package com.supinfo.todoapp.gui.helper;

import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.widget.EditText;

import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.controllers.TodoController;
import com.supinfo.todoapp.gui.activity.MainScreen;
import com.supinfo.todoapp.models.Dto.TodoList;

/*
 * AddTodo helper to manage all help messages on this matter
 */
public class AddTodoTaskHelper {
    public static void startAddingTodo(MainScreen mainScreen) {
        TodoList currentTodo =  TodoController.getDisplayTodoHelper().getCurrentDisplayedTodolist().clone();

        if(currentTodo==null)
        {
            DialogAlertTool.displayInfoPopup(mainScreen,"Error todolist","No current list set, this should not happen");
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(mainScreen);
        builder.setTitle("New task");
        builder.setMessage("You can write a new task here for list " +currentTodo.getId());

        EditText text = new EditText(mainScreen);
        text.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(text);

        builder.setPositiveButton("Add", (window, which) ->
        {
            if(text.getText() == null || text.getText().length() <=0 )
            {
                DialogAlertTool.displayInfoPopup(mainScreen,"Failed","Your text is too short !");
                return;
            }

            window.dismiss();
            currentTodo.addNewTodo(text.getText().toString());
            TodoController.get().sendUpdateSpecificTodoList(mainScreen,currentTodo);

        });
        builder.setNegativeButton("Cancel", (window, which) -> window.cancel());

        builder.show();
    }
}
