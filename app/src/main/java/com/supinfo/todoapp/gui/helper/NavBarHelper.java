package com.supinfo.todoapp.gui.helper;

import android.support.design.widget.NavigationView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.TextView;

import com.supinfo.todoapp.R;
import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.controllers.CredentialController;
import com.supinfo.todoapp.controllers.TodoController;
import com.supinfo.todoapp.gui.activity.MainScreen;
import com.supinfo.todoapp.models.Dto.TodoList;
import com.supinfo.todoapp.models.Dto.User;

/*
 * NavBar helper to manage all help messages on this matter
 */
public class NavBarHelper {

    public static void initializeCredidentialsDisplay(MainScreen mainScreen) {
        User u = CredentialController.get().getCurrentUser();

        if (u == null) {
            System.out.println("User data has not propely loaded !");
            DialogAlertTool.displayInfoPopup(mainScreen, "User error", "User data has not be loaded !");
            return;
        }

        TextView name = mainScreen.findViewById(R.id.userCalledName);
        String completeName = u.firstname + " " + u.lastname + " #" + u.username +" #"+u.id;
        name.setText(completeName);

        TextView mail = mainScreen.findViewById(R.id.userMail);
        mail.setText(u.email);
    }

    public static void refreshTodoBar() {
        NavigationView nav = MainScreen.instance.findViewById(R.id.nav_view);

        if (nav == null) {
            System.out.println("Error, nav bar not loaded");
            DialogAlertTool.displayInfoPopup(MainScreen.instance, "Can't display", "Sorry, nav bar not loaded, failed to load todo list");
            return;
        }

        Menu menu = nav.getMenu();
        menu.clear();

        SubMenu local = menu.addSubMenu("Private todo list");
        SubMenu shared = menu.addSubMenu("Shared todo list");

        for (TodoList todo : TodoController.get().getCurrentTodoList()) {
            MenuItem item;
            if (todo.isShared()) {
                item = shared.add(todo.getId());
            } else {
                item = local.add(todo.getId());
            }

            item.setIcon(R.drawable.ic_list);
        }
    }
}
