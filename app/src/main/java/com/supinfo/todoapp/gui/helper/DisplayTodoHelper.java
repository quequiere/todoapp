package com.supinfo.todoapp.gui.helper;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.supinfo.todoapp.R;
import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.controllers.TodoController;
import com.supinfo.todoapp.gui.activity.MainScreen;
import com.supinfo.todoapp.models.Dto.TodoList;

/*
 * DisplayTodo helper to manage all help messages on this matter
 */
public class DisplayTodoHelper {

    private TodoList currentDisplayedTodolist;

    public TodoList getCurrentDisplayedTodolist() {
        return currentDisplayedTodolist;
    }

    public void displayTodoList(MainScreen mainscreen, TodoList list) {
        System.out.println("Displaying a new todolist: "+list.getId() + "with todo: "+list.getTodoStringUnsplited());
        currentDisplayedTodolist = list;

        FloatingActionButton fab = mainscreen.findViewById(R.id.addList);
        fab.show();

        TableLayout table = mainscreen.findViewById(R.id.tableLayout);
        table.removeAllViews();

        TextView info = mainscreen.findViewById(R.id.todoinfo);
        TextView creator = mainscreen.findViewById(R.id.createdby);
        TextView lastupdate = mainscreen.findViewById(R.id.lastupdate);
        TextView invited = mainscreen.findViewById(R.id.invitedUser);
        TextView emtpyTodo = mainscreen.findViewById(R.id.emptytodo);

        String shared = list.isShared()?"(shared)":"(not shared)";

        info.setText("Current list: "+list.getId()+" "+shared);
        creator.setText("Created by: "+  list.getUsercreator());
        lastupdate.setText("Last update "+list.getLastupdate());
        invited.setText("User invited: " +list.getUserinvited());

        creator.setVisibility(View.VISIBLE);
        lastupdate.setVisibility(View.VISIBLE);

        int sharedVisi = list.isShared()?View.VISIBLE:View.GONE;
        invited.setVisibility(sharedVisi);

        int todoVisi = list.getTodo().length<=0?View.VISIBLE:View.GONE;
        emtpyTodo.setVisibility(todoVisi);

        for(String todo : list.getOrderedTodos())
        {
            TableRow currentRow = new TableRow(mainscreen);
            table.addView(currentRow);

            TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT,1.0f);
            param.topMargin=15;
            param.leftMargin=32;
            param.rightMargin=32;

            Button button = new Button(mainscreen);
            button.setAllCaps(false);
            button.setText(todo);

            button.setBackgroundResource(R.drawable.inputdraw);
            button.setGravity(Gravity.CENTER);
            button.setLayoutParams(param);

            button.setOnClickListener(v -> buttonClicked(mainscreen,v));

            currentRow.addView(button);
        }
    }

    public void displayTodoListById(MainScreen screen, String id)
    {
        for(TodoList todolist : TodoController.get().getCurrentTodoList())
        {
            if(todolist.getId().equals(id))
            {
                displayTodoList(screen,todolist);
                return;
            }
        }

        System.out.println("Error, list "+id+" doesn't exist");
    }

    public void selectTodoList(MainScreen mainscreen, MenuItem itemmenu)
    {
        displayTodoListById(mainscreen,itemmenu.getTitle().toString());
    }

    public void buttonClicked(MainScreen screen, View v)
    {
        Button b = (Button) v;

        AlertDialog.Builder builder = new AlertDialog.Builder(screen);
        builder.setTitle("Remove task");
        builder.setMessage("Do you really want to remove " +b.getText().toString());

        builder.setPositiveButton("Yes", (window, which) ->
        {
            if(currentDisplayedTodolist == null)
            {
                DialogAlertTool.displayInfoPopup(screen,"Display error","Failed to target task from todo list");
                return;
            }

            boolean finded = false;

           for(String task : currentDisplayedTodolist.getTodo())
           {
               if(task.equals(b.getText().toString()))
               {
                   finded = true;
                   break;
               }
           }

            if(!finded)
            {
                DialogAlertTool.displayInfoPopup(screen,"Remove task error","Can't find the task to remove");
                return;
            }

            if(currentDisplayedTodolist.getTodo().length<=1)
            {
                DialogAlertTool.displayInfoPopup(screen,"Failed","Sorry, but the REST API doesn't support the update of empty list!\nSee php api line 495 for more detail !");
            }
            else
            {
                currentDisplayedTodolist.removeTask(b.getText().toString());
                TodoController.get().sendUpdateSpecificTodoList(screen,currentDisplayedTodolist);
            }

            window.dismiss();

        });
        builder.setNegativeButton("No", (window, which) -> window.cancel());
        builder.show();
    }
}
