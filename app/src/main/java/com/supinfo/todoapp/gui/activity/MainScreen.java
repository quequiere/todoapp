package com.supinfo.todoapp.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.supinfo.todoapp.R;
import com.supinfo.todoapp.controllers.CredentialController;
import com.supinfo.todoapp.controllers.DatabaseController;
import com.supinfo.todoapp.controllers.TodoController;
import com.supinfo.todoapp.gui.helper.AddTodoTaskHelper;
import com.supinfo.todoapp.gui.helper.CreateTodoHelper;
import com.supinfo.todoapp.gui.helper.NavBarHelper;

import java.util.Timer;
import java.util.TimerTask;

/*
 * MainScreen view
 */
public class MainScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static MainScreen instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_main_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.addList);
        fab.hide();
        fab.setOnClickListener(view -> addButtonClick());

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        startCheckUpdate();
    }

    public void addButtonClick() {
        AddTodoTaskHelper.startAddingTodo(this);
    }

    @Override
    public void onBackPressed() {
        // When back is pressed, the menu on the back will be closed is open
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // Disable back cause we won't return on login screen
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.initializeMenuData();
        getMenuInflater().inflate(R.menu.main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            logoutPressed();
            return true;
        }
        else if(id == R.id.createlist)
        {
            CreateTodoHelper.createNewTodoCalled(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        TodoController.getDisplayTodoHelper().selectTodoList(this,item);

        //To hide the menu
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void initializeMenuData() {
        NavBarHelper.initializeCredidentialsDisplay(this);
        NavBarHelper.refreshTodoBar();
    }

    // Logout button pressed
    public void logoutPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("This will remove you cached credentials, are you sure ?");

        builder.setPositiveButton("Yes", (window, which) ->
        {
            DatabaseController.get().clearDatase();
            CredentialController.get().logout();
            CredentialController.tryLogout(this);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        });

        builder.setNegativeButton("Cancel", (window, which) -> window.cancel());
        builder.show();
    }

    // Check any update on todos
    public void startCheckUpdate()
    {
        MainScreen screen = this;

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                   System.out.println("Refresh all todo list");
                   TodoController.get().refreshedAllCachedTodoList(screen);
                });
            }
        },0,30000);
    }
}
