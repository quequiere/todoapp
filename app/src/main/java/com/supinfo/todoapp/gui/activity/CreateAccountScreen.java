package com.supinfo.todoapp.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;

import com.supinfo.todoapp.R;
import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.Tools.StringCheck;
import com.supinfo.todoapp.controllers.CredentialController;
import com.supinfo.todoapp.controllers.TodoController;

/*
 * View to create an account
 */
public class CreateAccountScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);

        final Button button = findViewById(R.id.createbutton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createAccount(v);
            }
        });
    }

    // Form fields verification
    public void createAccount(View view) {
        AppCompatButton button = (AppCompatButton) view;
        button.setEnabled(false);

        AppCompatEditText username = findViewById(R.id.createusername);
        AppCompatEditText password1 = findViewById(R.id.createpassword);
        AppCompatEditText password2 = findViewById(R.id.createpassword2);
        AppCompatEditText firstname = findViewById(R.id.createfirstname);
        AppCompatEditText lastname = findViewById(R.id.createlastname);
        AppCompatEditText email = findViewById(R.id.createmail);


        if (username.getText().length() <= 0
                || password1.getText().length() <= 0
                || password2.getText().length() <= 0
                || firstname.getText().length() <= 0
                || lastname.getText().length() <= 0
                || email.getText().length() <= 0) {
            DialogAlertTool.displayInfoPopup(this, "Missing data", "Sorry, you need to complete all fields !");
            button.setEnabled(true);
            return;
        }

        if (StringCheck.isTooLong(username.getText().toString())) {
            DialogAlertTool.displayInfoPopup(this, "Error data", "Sorry, username is too long");
            button.setEnabled(true);
            return;
        }

        if (StringCheck.isTooLong(firstname.getText().toString())) {
            DialogAlertTool.displayInfoPopup(this, "Error data", "Sorry, firstname is too long");
            button.setEnabled(true);
            return;
        }

        if (StringCheck.isTooLong(lastname.getText().toString())) {
            DialogAlertTool.displayInfoPopup(this, "Error data", "Sorry, lastname is too long");
            button.setEnabled(true);
            return;
        }

        if (StringCheck.isEmailValid(email.getText().toString())) {
            DialogAlertTool.displayInfoPopup(this, "Error data", "Sorry, email has invalid format");
            button.setEnabled(true);
            return;
        }

        if (!password1.getText().toString().equals(password2.getText().toString())) {
            DialogAlertTool.displayInfoPopup(this, "Error data", "Password doesn't matching !");
            button.setEnabled(true);
            return;
        }

        // If you are here GG ! that's means that all local test passed, now try to register and login

        // Create credential controller to add interceptors

        CredentialController.tryRegister(this,
                username.getText().toString(),
                password1.getText().toString(),
                firstname.getText().toString(),
                lastname.getText().toString(),
                email.getText().toString(),
                object -> {
                    if (object.success) {
                        System.out.println("User account was created !");
                        TodoController.get().refreshedAllCachedTodoList(this);
                        Intent intent = new Intent(this, MainScreen.class);
                        startActivity(intent);
                    } else {
                        DialogAlertTool.displayInfoPopup(this, "Register error", "Sorry we failed to register yourself !\nMessage: " + object.message);
                        button.setEnabled(true);

                        return;
                    }
                });
    }
}
