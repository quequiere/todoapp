package com.supinfo.todoapp.gui.helper;

import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.widget.EditText;

import com.supinfo.todoapp.Tools.DialogAlertTool;
import com.supinfo.todoapp.controllers.TodoController;
import com.supinfo.todoapp.gui.activity.MainScreen;

/*
 * CreateTodo helper to manage all help messages on this matter
 */
public class CreateTodoHelper {

    public static void createNewTodoCalled(MainScreen mainScreen)
    {
        if(TodoController.get().getCurrentTodoList().size()>=50)
        {
            DialogAlertTool.displayInfoPopup(mainScreen,"Failed","Sorry but you can't create more than 50 todo list !");
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(mainScreen);
        builder.setTitle("New todo list");
        builder.setMessage("Write your friend user name here:");

        EditText text = new EditText(mainScreen);
        text.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(text);

        builder.setPositiveButton("Create", (window, which) ->
        {
            if(text.getText() == null || text.getText().length() <=0 )
            {
                DialogAlertTool.displayInfoPopup(mainScreen,"Failed","This friend name is too short !");
                return;
            }

            window.dismiss();
            TodoController.get().createNewTodoListShared(mainScreen,text.getText().toString());

        });

        builder.setNegativeButton("Cancel", (window, which) -> window.cancel());
        builder.show();
    }
}
