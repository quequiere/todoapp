package com.supinfo.todoapp.models.Dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// ToDoList model
public class TodoList {

    // Properties
    private String id;
    private String lastupdate;
    private String usercreator;
    private String userinvited;
    private String todo;

    // Constructor
    public TodoList(String id, String lastupdate, String usercreator, String userinvited, String todo) {
        this.id = id;
        this.lastupdate = lastupdate;
        this.usercreator = usercreator;
        this.userinvited = userinvited;
        this.todo = todo;
    }

    // Methods
    public boolean isShared() {
        return !(userinvited == null);
    }
    public String getId() {
        return id;
    }
    public String getLastupdate() {
        return lastupdate;
    }
    public String getUsercreator() {
        return usercreator;
    }
    public String getUserinvited() {
        return userinvited;
    }

    public String[] getTodo() {
        if(todo == null || todo.length() <=0)
        {
           return new String[0];
        }

        return todo.split(", ");
    }

    public List<String> getOrderedTodos()
    {
        List<String> liste = Arrays.asList(this.getTodo());
        Collections.sort(liste,new ignoreCaseSort());

       return liste;
    }

    public class ignoreCaseSort implements Comparator<Object> {
        public int compare(Object o1, Object o2) {
            String s1 = (String) o1;
            String s2 = (String) o2;
            return s1.toLowerCase().compareTo(s2.toLowerCase());
        }
    }

    public String getTodoStringUnsplited()
    {
        return unSplitTodo(this.getTodo());
    }

    private static String unSplitTodo(String[] list)
    {
        String total = null;

        for(String todo : list)
        {
            if(total==null)
                total=todo;
            else
                total+=(", "+todo);
        }

        if(total==null)
            return "";

        return total;
    }

    public void addNewTodo(String s)
    {
        if(todo==null || todo.length()<=0)
        {
            todo=s;
        }
        else
        {
            todo+=(", "+s);
        }
    }

    public void removeTask(String t)
    {
        ArrayList<String> cleaned = new ArrayList<>();
        for(int x = 0; x< this.getTodo().length;x++)
        {
            String scan = this.getTodo()[x];
            if(!scan.equals(t))
            {
                cleaned.add(scan);
            }
        }

        String[] result = cleaned.toArray(new String[cleaned.size()]);

        this.todo = unSplitTodo(result);
    }

    public TodoList clone()
    {
        TodoList clone = new TodoList(this.id,this.lastupdate,this.usercreator,this.userinvited,this.todo);
        return clone;
    }
}
