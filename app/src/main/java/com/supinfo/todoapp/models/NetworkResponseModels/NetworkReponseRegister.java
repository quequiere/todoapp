package com.supinfo.todoapp.models.NetworkResponseModels;

/*
 * Manage network response (success / failure) after a registration request
 */
public class NetworkReponseRegister extends NetworkResponseSuccess {
    public int id;
    public String username;
    public String password;
    public String firstname;
    public String lastname;
    public String email;
}
