package com.supinfo.todoapp.models.Dto;

// Credentials model
public class CredentialPair {
    // Properties
    public String username;
    public String password;

    // Constructor
    public CredentialPair(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
