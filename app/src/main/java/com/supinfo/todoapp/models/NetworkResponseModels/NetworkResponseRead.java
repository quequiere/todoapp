package com.supinfo.todoapp.models.NetworkResponseModels;

import com.supinfo.todoapp.models.Dto.TodoList;

/*
 * Manage network response (success / failure) after a simple read request
 */
public class NetworkResponseRead extends NetworkResponseSuccess {
    public TodoList todolistResponse;
}
