package com.supinfo.todoapp.models.NetworkResponseModels;

/*
 * Manage network success response
 */
public abstract class NetworkResponseSuccess {
    public boolean success = true;
    public String message;
}
