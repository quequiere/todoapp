package com.supinfo.todoapp.models.Dto;

// User model
public class User {
    // Properties
    public int id;
    public String username;
    public String password;
    public String firstname;
    public String lastname;
    public String email;

    // Constructor
    public User(int id, String username, String password, String firstname, String lastname, String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }
}
