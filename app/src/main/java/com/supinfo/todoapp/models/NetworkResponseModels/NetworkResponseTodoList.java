package com.supinfo.todoapp.models.NetworkResponseModels;

import com.supinfo.todoapp.models.Dto.TodoList;

import java.util.ArrayList;

/*
 * Manage network response (success / failure) after a todolist request
 */
public class NetworkResponseTodoList extends NetworkResponseSuccess {
    public ArrayList<TodoList> list;
}
