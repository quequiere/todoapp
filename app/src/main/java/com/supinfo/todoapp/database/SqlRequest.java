package com.supinfo.todoapp.database;

/*
 * Base SQL Request to declare the user table
 */
public class SqlRequest {

    public static final String dbName = "suptodo";
    public static final String tbName = "userdata";

    public static final String CREATE_USER_TABLE = "CREATE TABLE "+tbName+" (" +
            "username VARCHAR(100), " +
            "password VARCHAR(100))";
}
