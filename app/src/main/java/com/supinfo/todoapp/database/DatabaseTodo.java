package com.supinfo.todoapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Clean user table or create it
 */
public class DatabaseTodo extends SQLiteOpenHelper {
    public DatabaseTodo(Context context) {
        super(context, SqlRequest.dbName, null, 6);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(SqlRequest.CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       System.out.println( "Upgrading database V" + oldVersion + " to V"+newVersion + ", old data removed.");
        db.execSQL("DROP TABLE IF EXISTS " + SqlRequest.tbName);
        onCreate(db);
    }
}
