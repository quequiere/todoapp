package com.supinfo.todoapp.models;


import com.supinfo.todoapp.models.Dto.TodoList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TodoListTest {

    @DisplayName("Check if todo in TodoList is parsed in the good way")
    @ParameterizedTest(name = "{index} => Parsing {0} and should return {1} output")
    @MethodSource("generatedTodoList")
    void testTodoParsing(String todo, int expected)
    {
        TodoList todolist = new TodoList("1","2018-12-11 10:50:16","78","2",todo);
        assertEquals(todolist.getTodo().length,expected);

    }
    private static Stream<Arguments> generatedTodoList() {
        return Stream.of(
                Arguments.of("thé, café, truc", 3),
                Arguments.of("croquette, aller au bureau, faire les courses, se coucher", 4),
                Arguments.of("chat,espace, autre", 2)
        );
    }



    @DisplayName("Check if userinvited in TodoList is parsed in the good way")
    @ParameterizedTest(name = "{index} => message=''{0}''")
    @CsvSource({
            "42,true",
            "test,true",
            ",false"
    })
    void testUserInvitedParsing(String userInvited, boolean expected)
    {
        TodoList todolist = new TodoList("1","2018-12-11 10:50:16","78",userInvited,"");
        assertEquals(todolist.isShared(),expected);

    }

}
